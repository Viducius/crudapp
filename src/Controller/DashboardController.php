<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(UserPasswordEncoderInterface $encoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $request = Request::createFromGlobals();
        $message = [];

        if ($request->isMethod('post')) {        
            $em = $this->getDoctrine()->getManager();
            $userRepo = $em->getRepository(User::class);    
            $id = $this->getUser()->getId();    
            $user = $userRepo->find($id);
            
            if (!password_verify($request->request->get('currentPassword'), $user->getPassword())) {
                $message =  ['message' => 'Current password doesn\'t match.', 'type' => 'danger'];
                return $this->render('dashboard/index.html.twig', ['message' => $message]);
            }

            if ($request->request->get('newPassword') !== $request->request->get('confirmNewPassword')) {
                $message =  ['message' => 'New password doesn\'t match the repeated password.', 'type' => 'danger'];
                return $this->render('dashboard/index.html.twig', ['message' => $message]);
            }
            $user->setUsername($request->request->get('username'));
            $user->setEmail($request->request->get('email'));
            $user->setPassword($encoder->encodePassword($user, $request->request->get('newPassword')));

            $em->flush();

            $message = ['message' => 'Account settings have been changed.', 'type' => 'success'];
        }

        return $this->render('dashboard/index.html.twig', ['message' => $message]);
    }

    /**
     * @Route("/landing", name="landing")
     */
    public function landing()
    {
        return $this->render('dashboard/landing.html.twig');
    }
}
