<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* dashboard/index.html.twig */
class __TwigTemplate_99bdd550e5620ae405a0e0a646441da157f6743da3d6c7901f70c29913e09633 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "dashboard/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "dashboard/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "dashboard/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "crudApp - Dashboard";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"row align-items-center vh-100\">
        <div class=\"col-4 p-5 span2 text-center\">
            <p><a class=\"btn btn-primary mt-4 w-50\" href=\"/logout\">Logout</a></p>
            <p><a class=\"btn btn-danger mt-4 w-50\" href=\"/delete_user/";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "user", [], "any", false, false, false, 9), "id", [], "any", false, false, false, 9), "html", null, true);
        echo "\">Delete Account</a></p>
        </div>
        <div class=\"col-8 border-left p-5\">
            <form method=\"post\">
                <h2 class=\"mb-5\">You are now logged in as ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 13, $this->source); })()), "user", [], "any", false, false, false, 13), "username", [], "any", false, false, false, 13), "html", null, true);
        echo "</h2>

                ";
        // line 15
        if ((isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 15, $this->source); })())) {
            // line 16
            echo "                    <div class=\"alert alert-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 16, $this->source); })()), "type", [], "any", false, false, false, 16), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 16, $this->source); })()), "message", [], "any", false, false, false, 16), "html", null, true);
            echo "</div>
                ";
        }
        // line 18
        echo "                <h3>Change account settings</h3>
                <div class=\"form-group\">
                    <label for=\"inputUsername\">Username <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"text\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "user", [], "any", false, false, false, 21), "username", [], "any", false, false, false, 21), "html", null, true);
        echo "\" name=\"username\" id=\"inputUsername\" class=\"form-control\" required autofocus>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputEmail\">Email address <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"email\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "email", [], "any", false, false, false, 25), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\" class=\"form-control\" required autofocus>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputCurrentPassword\">Current Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"currentPassword\" id=\"inputCurrentPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputNewPassword\">New Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"newPassword\" id=\"inputNewPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputConfirmNewPassword\">Repeat New Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"confirmNewPassword\" id=\"inputConfirmNewPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <button class=\"btn btn-primary form-control\" type=\"submit\">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "dashboard/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 25,  120 => 21,  115 => 18,  107 => 16,  105 => 15,  100 => 13,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}crudApp - Dashboard{% endblock %}

{% block body %}
    <div class=\"row align-items-center vh-100\">
        <div class=\"col-4 p-5 span2 text-center\">
            <p><a class=\"btn btn-primary mt-4 w-50\" href=\"/logout\">Logout</a></p>
            <p><a class=\"btn btn-danger mt-4 w-50\" href=\"/delete_user/{{ app.user.id }}\">Delete Account</a></p>
        </div>
        <div class=\"col-8 border-left p-5\">
            <form method=\"post\">
                <h2 class=\"mb-5\">You are now logged in as {{ app.user.username }}</h2>

                {% if message %}
                    <div class=\"alert alert-{{ message.type }}\">{{ message.message }}</div>
                {% endif %}
                <h3>Change account settings</h3>
                <div class=\"form-group\">
                    <label for=\"inputUsername\">Username <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"text\" value=\"{{ app.user.username }}\" name=\"username\" id=\"inputUsername\" class=\"form-control\" required autofocus>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputEmail\">Email address <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"email\" value=\"{{ app.user.email }}\" name=\"email\" id=\"inputEmail\" class=\"form-control\" required autofocus>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputCurrentPassword\">Current Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"currentPassword\" id=\"inputCurrentPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputNewPassword\">New Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"newPassword\" id=\"inputNewPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"inputConfirmNewPassword\">Repeat New Password <span style=\"color:#FF0000;\">*</span></label>
                    <input type=\"password\" name=\"confirmNewPassword\" id=\"inputConfirmNewPassword\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <button class=\"btn btn-primary form-control\" type=\"submit\">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
{% endblock %}
", "dashboard/index.html.twig", "C:\\Users\\Jesse\\Desktop\\dev\\crudApp\\crudApp\\templates\\dashboard\\index.html.twig");
    }
}
